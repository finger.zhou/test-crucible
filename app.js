'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const compression = require('compression');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware'); 
const app = express();
const router = express.Router();

// gzip compression
app.use(compression());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(awsServerlessExpressMiddleware.eventContext());

router.get('/hello', (req, res) => {
    res.json({"say": "Hello World"})
});

router.get('/users', (req, res) => {
    res.json(users);
});

router.get('/users/:userId', (req, res) => {
    const user = getUser(req.params.userId);
    if(!user) {
        return res.status(404).json({});
    }
    return res.json(user);
});

router.post('/users', (req, res) => {
    const user = {
        id: ++userIdCounter,
        name: req.body.name
    };
    users.push(user);
    res.status(201).json(user);
});

router.put('/users/:userId', (req, res) => {
    const user = getUser(req.params.userId);
    console.log(req.params.userId, user);
    if (!user) {
        return res.status(404).json({});
    }
    user.name = req.body.name;
    res.json(user);
});

router.delete('/users/:userId', (req, res) => {
    const userIndex = getUserIndex(req.params.userId);
    if (userIndex === -1) {
        return res.status(404).json({});
    }
    users.splice(userIndex, 1);
    res.json(users);
});

const users = [{
    id: 1,
    name: 'Joe'
}, {
    id: 2,
    name: 'Jame'
}];

let userIdCounter = users.length;

const getUser = (userId) => users.find(u => u.id === parseInt(userId));
const getUserIndex = (userId) => users.findIndex(u => u.id === parseInt(userId));

app.use('/', router);

module.exports = app;